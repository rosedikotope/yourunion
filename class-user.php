<?php
class User
{
    private $db;
 
    function __construct($con)
    {
      $this->db = $con;
    }
 
    public function register($uname,$usurname,$udob,$ucell,$ulanguage,$umail,$upass)
    {
         $is_registered = false;

         $stmt = $this->db->prepare("SELECT name,email FROM users WHERE name=:uname OR email=:umail");
         $stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
         $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
         if($row['name']==$uname) {
            throw new Exception("Username already taken!");
         }
         else if($row['email']==$umail) {
            throw new Exception("Email address already taken!");
         }
         else
         {
           $new_password = password_hash($upass, PASSWORD_DEFAULT);
   
           $stmt = $this->db->prepare("INSERT INTO users(name,surname,dob,cell,language,email,password) 
                                       VALUES(:uname, :usurname, :udob, :ucell, :ulanguage, :umail, :upass)");

           $stmt->bindparam(":uname", $uname);
           $stmt->bindparam(":usurname", $usurname);
           $stmt->bindparam(":udob", $udob);
           $stmt->bindparam(":ucell", $ucell);
           $stmt->bindparam(":ulanguage", $ulanguage);
           $stmt->bindparam(":umail", $umail);
           $stmt->bindparam(":upass", $new_password);            
           $stmt->execute();

           if( $stmt->rowCount() > 0 ){
              $is_registered = true;
           }

         }
         
         return $is_registered; 
    }
 
    public function login($umail,$upass)
    {
        $is_loggedin = false;

          $stmt = $this->db->prepare("SELECT id, email, password FROM users WHERE email=:umail LIMIT 1");
          $stmt->execute(array(':umail'=>$umail));
          $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

          if($stmt->rowCount() > 0)
          {
             if(password_verify($upass, $userRow['password']))
             {
                $_SESSION['user_session'] = $userRow['id'];
                $is_loggedin = true;
             }
          }
        

        return $is_loggedin;
    }
           
   public function logout()
   { 
     session_destroy();
     unset($_SESSION['user_session']);
   }
 
   public function isLoggedin()
   {
      if(isset($_SESSION['user_session']))
      {
         return true;
      }
   }

   public function getUser($user_id = null){
      $user_id = isset($user_id) && !empty($user_id) ? $user_id : $_SESSION['user_session'];
      $stmt = $this->db->prepare("SELECT * FROM users WHERE id=:user_id");
      $stmt->execute(array(":user_id"=>$user_id));
      return $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
   }

    public function addBeneficiary($uidno,$uname,$usurname,$memeber_id)
    {
         $is_registered = false;

         $stmt = $this->db->prepare("SELECT rsa_idnumber FROM beneficiary WHERE rsa_idnumber=:uidno");
         $stmt->execute(array(':uidno'=>$uidno));
         $row = $stmt->fetch(PDO::FETCH_ASSOC);
  
         if($row['rsa_idnumber']==$uidno) {
            throw new Exception("Beneficiary already added.");
         }
         else
         {
           $stmt = $this->db->prepare("INSERT INTO beneficiary(rsa_idnumber,first_name,last_name,memeber_id) 
                                       VALUES(:rsa_idnumber, :first_name, :last_name, :memeber_id)");

           $stmt->bindparam(":rsa_idnumber", $uidno);
           $stmt->bindparam(":first_name", $uname);
           $stmt->bindparam(":last_name", $usurname);
           $stmt->bindparam(":memeber_id", $memeber_id);
           $stmt->execute();

           if( $stmt->rowCount() > 0 ){
              $is_registered = true;
           }

         }
         
         return $is_registered; 
    }
 
    public function getMemberBeneficiaries($memeber_id = null)
    {
      $beneficiaries = [];
      $memeber_id = isset($memeber_id) && !empty($memeber_id) ? $memeber_id : $_SESSION['user_session'];
      $stmt = $this->db->prepare("SELECT * FROM beneficiary WHERE memeber_id=:memeber_id");
      $stmt->execute(array(":memeber_id"=>$memeber_id));

      $beneficiaries = $stmt->fetchAll(PDO::FETCH_ASSOC);

      return $beneficiaries;
    }
}
