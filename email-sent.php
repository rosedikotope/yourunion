<!DOCTYPE HTML> 
<html>
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

      <title>Contact Us | Your Union</title>

       <!-- CSS  -->  
       <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />    
       <link href="css/style.css" type="text/css" rel="stylesheet" />
       <link href="css/login.css" type="text/css" rel="stylesheet" />

       <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      
     

</head>

<body>
  

<div class="wrapper">

<header>

       <span class="glyphicon glyphicon-fire" aria-hidden="true"></span> <a href="index.php">your union</a>

            <div class="menu" >
                <ul>
                       <li><a href="index.php">Home</a></li>
                      <li><a href="sign-up.php">Sign Up</a></li>
                      <li><a href="contact.php">Contact</a></li>
                    </ul>
              </div>
</header> 



<div class="container">
<?php
 
if(isset($_POST['email'])) {
  
 
    $email_to = "rosedikotope@gmail.com";
    $email_subject = "Member Message";
 
    function died($error) {
 
        echo $error."<br /><br />";
        die();
 
    }
     
 
    if(!isset($_POST['name']) ||
       !isset($_POST['email']) || 
       !isset($_POST['message'])) {
 
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }   
 
    $name = $_POST['name']; 
    $email = $_POST['email']; 
    $message = $_POST['message']; 
 
     
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email)) {
 
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
 
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$name)) {
 
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
 
  }

 
  if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
 
    $email_message = "Form details below.\n\n";
 
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }
 
     
    $email_message .= "Name: ".clean_string($name)."\n"; 
    $email_message .= "Email: ".clean_string($email)."\n";
    $email_message .= "Message: ".clean_string($message)."\n";
 
     
  
 
// create email headers
 
$headers = 'From: '.$email."\r\n".
 
'Reply-To: '.$email."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
 
?>
 
 
 
<!-- include your own success html here -->
 
 
 <br><br><br>
<p>Thank you for contacting us. We will be in touch with you very soon.</p>
 
 
 
<?php
 
}
 
?>
</div>
</div>
</body>
</html>