<?php
session_start();

$DB_host = "localhost";
$DB_user = "root";
$DB_pass = "";
$DB_name = "yourunion_membership";

try
{
     $con = new PDO('mysql:host=localhost;dbname=yourunion_membership', 'root', '');
     $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
     echo $e->getMessage();
}

include_once 'class-user.php';
$user = new User($con);

?>