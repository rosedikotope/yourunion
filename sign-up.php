<?php
require_once 'dbconfig.php';

if($user->isLoggedin())
{
  header('Location:home.php');
  exit;
}

$fname     = '';
$umail     = '';
$upass     = '';
$lname     = '';
$dob       = '';
$cell      = '';
$language  = '';

if(isset($_POST) && !empty($_POST))
{

   $fname     = trim($_POST['name']);
   $umail     = trim($_POST['email']);
   $upass     = trim($_POST['password']);
   $lname     = trim($_POST['surname']);
   $dob       = trim($_POST['dob']);
   $cell      = trim($_POST['cell']);
   $language  = trim($_POST['language']);

   $error = [];

    try
    {
        if($user->register($fname,$lname,$dob,$cell,$language,$umail,$upass)) 
        {
            header('Location:index.php?joined');
        }
        else{
          $error[] = 'User failed to register';
        }
   }
   catch(Exception $e)
   {
      error_log($e->getMessage());
      $error[] = 'Oops some strange error just happened!';
   }
  
}

?>
<!DOCTYPE HTML> 
<html>
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

      <title>Sign Up | Your Union</title>

       <!-- CSS  -->  
       <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />    
       <link href="css/style.css" type="text/css" rel="stylesheet" />
       <link href="css/login.css" type="text/css" rel="stylesheet" />

       <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      
     

</head>

<body>
  

<div class="wrapper">

<header>

       <span class="glyphicon glyphicon-fire" aria-hidden="true"></span> <a href="index.php">your union</a>

            <div class="menu" >
                <ul>
                       <li><a href="index.php">Home</a></li>
                      <li><a href="sign-up.php">Sign Up</a></li>
                      <li><a href="contact.php">Contact</a></li>
                    </ul>
              </div>
</header> 


<div class="container">
<div class="row">
    <div class="col-sm-6">
<form class="form-horizontal" method="POST">
 <?php
  if(isset($error))
  {
     foreach($error as $error)
     {
        ?>
        <div class="alert alert-danger">
            <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
        </div>
        <?php
     }
  }
 ?>

<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="Name" name="name" value="<?=$fname?>" required>
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Surname</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="Surname" name="surname" value="<?=$lname?>" required>
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">DOB</label>
    <div class="col-sm-10">
      <input type="date" class="form-control" id="inputPassword3" placeholder="DOB" name="dob" value="<?=$dob?>" required>
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Mobile</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="Cell" name="cell" value="<?=$cell?>" required>
    </div>
  </div>

 <div class="form-group">
<label for="inputPassword3" class="col-sm-2 control-label">Language</label>
<div class="col-sm-10">
<select class="form-control" id="dropdown" name="language">
  <option>English</option>
  <option>Afrikaans</option>
  <option>iSizulu</option>
  <option>Setswana</option>
  <option>XiTsonga</option>
  <option>Other</option>
</select>
</div>
</div>


  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password" required>
    </div>
  </div>
 
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Sign Up</button>
    </div>
  </div>
</form>
</div>

<div class="col-sm-6">
</div>
</div>


</div>

  <div class="footer">
<p> © 2016 your union. All Rights Reserved. </p>
  </div>

  

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>