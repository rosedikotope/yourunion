<?php
require_once 'dbconfig.php';

if($user->isLoggedin())
{
  header('Location:home.php');
  exit;
}

$error = null;

if(isset($_POST) && !empty($_POST))
{
    $umail = $_POST['email'];
    $upass = $_POST['password'];
  
   if($user->login($umail,$upass))
   {
    header('Location:home.php');
    exit;
   }
   else
   {
    $error = "Wrong Details !";
   } 
}

?>

<!DOCTYPE HTML> 
<html>
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

      <title>Home | Your Union</title>

       <!-- CSS  -->  
       <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />    
       <link href="css/style.css" type="text/css" rel="stylesheet" />
       <link href="css/login.css" type="text/css" rel="stylesheet" />

       <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      
     

</head>

<body>
  

<div class="wrapper">

<header>

       <span class="glyphicon glyphicon-fire" aria-hidden="true"></span> <a href="index.php">your union</a>

            <div class="menu" >
                <ul>
                      <li><a href="index.php">Home</a></li>
                      <li><a href="sign-up.php">Sign Up</a></li>
                      <li><a href="contact.php">Contact</a></li>
                    </ul>
              </div>
</header> 



<div class="container">
<div class="row">
    <div class="col-sm-6">
      <img src="image/union2.png" />
    </div>

    <div class="col-sm-6">
      <div class="login">
  <div class="login-triangle"></div>
  
  <h2 class="login-header">Log in</h2>

  <form class="login-container" action="" method="POST">

  <?php
    if(isset($error))
    {
          ?>
          <div class="alert alert-danger">
              <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
          </div>
          <?php
    }
    elseif(isset($_GET['joined']))
    {
         ?>
         <div class="alert alert-info">
              <i class="glyphicon glyphicon-log-in"></i> &nbsp; Successfully registered! Please login.
         </div>
         <?php
    }
    ?>
    <p><input type="email" placeholder="Email" name="email"></p>
    <p><input type="password" placeholder="Password" name="password"></p>
    <p><input type="submit" value="Log in" name="submit"></p>
  </form>
</div>
    
   

    </div>
 </div>

</div>

  <div class="footer">
<p> © 2016 your union. All Rights Reserved. </p>
  </div>

  

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>