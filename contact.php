
<!DOCTYPE HTML> 
<html>
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

      <title>Contact Us | Your Union</title>

       <!-- CSS  -->  
       <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />    
       <link href="css/style.css" type="text/css" rel="stylesheet" />
       <link href="css/login.css" type="text/css" rel="stylesheet" />

       <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      
     

</head>

<body>
  

<div class="wrapper">

<header>

       <span class="glyphicon glyphicon-fire" aria-hidden="true"></span> <a href="index.php">your union</a>

            <div class="menu" >
                <ul>
                       <li><a href="index.php">Home</a></li>
                      <li><a href="sign-up.php">Sign Up</a></li>
                      <li><a href="contact.php">Contact</a></li>
                    </ul>
              </div>
</header> 



<div class="container">
  <h3 class="text-head">Contact Us</h3>
            <p class="box-desc">Get in touch with us by filling out the form below and we will contact you back soon.</p>
              
<div class="row">
    <div class="col-sm-6">

    <form class="form-horizontal" method="POST" action="email-sent.php">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Name" name="name" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="inputPassword3" placeholder="Email" name="email" required="">
    </div>
  </div>
<div class="form-group">
<label for="inputEmail3" class="col-sm-2 control-label">Message</label>
    <div class="col-sm-10">
  <textarea class="form-control" rows="10" name="message" required=""></textarea>
  </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Send Message</button>
    </div>
  </div>

</form>
</div>
 <div class="col-sm-6"></div>
</div>
</div>
  <div class="footer">
<p> © 2016 your union. All Rights Reserved. </p>
  </div>

  

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>
