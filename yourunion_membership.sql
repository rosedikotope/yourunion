-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2016 at 01:41 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yourunion_membership`
--

-- --------------------------------------------------------

--
-- Table structure for table `beneficiary`
--

CREATE TABLE `beneficiary` (
  `id` int(11) NOT NULL,
  `rsa_idnumber` int(13) NOT NULL,
  `first_name` int(50) NOT NULL,
  `last_name` int(50) NOT NULL,
  `memeber_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beneficiary`
--

INSERT INTO `beneficiary` (`id`, `rsa_idnumber`, `first_name`, `last_name`, `memeber_id`) VALUES
(1, 2147483647, 0, 0, 2),
(2, 2147483647, 0, 0, 2),
(3, 0, 0, 0, 2),
(4, 2147483647, 0, 0, 2),
(5, 2147483647, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `cell` varchar(50) NOT NULL,
  `language` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `dob`, `cell`, `language`, `email`, `password`) VALUES
(1, 'Ed', 'Dikotope', '1988-08-17', '736507846', '', 'edikotope@gmail.com', '$2y$10$AtbQyKyzo7Sd/slJ27JHWeaxflJluLkZ5bc8yomoBBTgJZxdxZEI6'),
(2, 'Rose', 'Dikotope', '2016-07-28', '0736507846', '', 'rosedikotope@gmail.com', '$2y$10$2LVcs0g9vYU5O0Dj.hOs5.Fp7pRZ3XSa468XoqZ7ZBin9KDLJro/6'),
(3, 'Kopano', 'Dikotope', '2016-07-01', '0823654123', 'iSizulu', 'kopano@gmail.com', '$2y$10$ofF01qoemnOfBbiKubEQSODmQW.8NKGGmpx3d3vFIm812G/oNVYOe'),
(4, 'Ruby', 'On Rails', '2016-07-16', '07369854447', 'English', 'default@english.com', '$2y$10$euiG2ntY0MUwWp5FdNEAzO96QfDj7hohpGjn1z.eGKfH.cNe9PEOy'),
(5, 'Thuso', 'Mooke', '2016-07-14', '0736507845', 'XiTsonga', 'x@x.x', '$2y$10$SLHHhIM3pxw861Eb19Z0DOuTzLeOulruxudDFz1LoR4VqGLGHhUsi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beneficiary`
--
ALTER TABLE `beneficiary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beneficiary`
--
ALTER TABLE `beneficiary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
