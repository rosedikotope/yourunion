<?php
include_once 'dbconfig.php';

if($user->isLoggedin() !== true)
{
	header('Location:index.php');
	exit;
}

$userRow = $user->getUser();
$userBeneficiaries = $user->getMemberBeneficiaries();

$idno     = '';
$fname    = '';
$lname    = '';


if(isset($_POST) && !empty($_POST))
{
   $idno      = trim($_POST['idno']);
   $fname     = trim($_POST['name']);
   $lname     = trim($_POST['surname']);
   $member_id = $userRow['id'];

   $error = [];

    try
    {
        if($user->addBeneficiary($idno,$fname,$lname,$member_id)) 
        {
            header('Location:home.php?added');
            exit;
        }
        else{
          $error[] = 'User failed to register';
        }
   }
   catch(Exception $e)
   {
      error_log($e->getMessage());
      $error[] = 'Oops some strange error just happened!';
   }
  
}

?>


<!DOCTYPE HTML> 
<html>
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

      <title>Welcome - <?= $userRow['name'] ?></title>

       <!-- CSS  -->  
       <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />    
       <link href="css/style.css" type="text/css" rel="stylesheet" />
       <link href="css/login.css" type="text/css" rel="stylesheet" />
       <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      
      <script>        
           function phoneno(){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }
       </script>

</head>

<body>
  

<div class="wrapper">

<header>

       <span class="glyphicon glyphicon-fire" aria-hidden="true"></span> <a href="index.php">your union</a>

            <div class="menu" >
                <ul>
                      <li><a href="index.php">Home</a></li>
                      <li><a href="sign-out.php"><i class="glyphicon glyphicon-log-out"></i>Logout</a></li>
                      <li><a href="contact.php">Contact</a></li>
                    </ul>
              </div>
</header> 



<div class="container">
<div class=welcome>
<h1>Welcome - <?= $userRow['name'] ?></h1>
</div>

<div class="row">
    <div class="col-sm-4">


<h3> Member Details</h3><br>
   <table class="table">
 
  <tr>
    <td>Name</td>
    <td><?= $userRow['name'] ?></td>
   
  </tr>
  <tr>
    <td>Surname</td>
    <td><?= $userRow['surname'] ?></td>
    
  </tr>
  <tr>
    <td>Mobile</td>
    <td><?= $userRow['cell'] ?></td>
   
  </tr>
  <tr>
    <td>Date of Birth</td>
    <td><?= $userRow['dob'] ?></td>
    
  </tr>
  <tr>
    <td>E-mail</td>
    <td><?= $userRow['email'] ?></td>
      </tr>
 
</table>


    </div>


  <div class="col-sm-4">


  <h3> Add Beneficiary</h3><br>
  <form class="form-horizontal" method="POST">

 <?php
    if(isset($error))
    {
          ?>
          <div class="alert alert-danger">
              <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error[0]; ?> !
          </div>
          <?php
    }
    elseif(isset($_GET['added']))
    {
         ?>
         <div class="alert alert-info">
              <i class="glyphicon glyphicon-ok"></i> &nbsp; Successfully added!
         </div>
         <?php
    }
    ?>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">ID No.</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="phone" onkeypress="phoneno()" maxlength="13" placeholder="ID Number" name="idno"  required>
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="Name" name="name" required>
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Surname</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="Surname" name="surname"  required>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Add Beneficiary</button>
    </div>
  </div>
</form>

</div>

<div class="col-sm-4">

<h3> Your Beneficiaries</h3><br>

  <?php if(!empty($userBeneficiaries)) { ?>
  <table class="table">
    <tr><th>ID</th><th>Name</th></tr>
    <?php foreach($userBeneficiaries as $row) { ?>
    <tr>
      <tr>
        <td><?=$row['rsa_idnumber']?></td>
        <td><?=$row['first_name'] . ' ' . $row['last_name']?></td>
      </tr>
    </tr>
    <?php } ?>
  </table>
  <?php } else { ?>
    <span>No beneficiaries</span>
  <?php } ?>
</div>
</div>
</div>

  <div class="footer">
	<p> © 2016 your union. All Rights Reserved. </p>
  </div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>